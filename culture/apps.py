from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CultureConfig(AppConfig):
    name = 'culture'
    verbose_name = _('Панель культур')
