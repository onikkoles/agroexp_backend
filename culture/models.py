from django.db import models
from django.utils.translation import gettext_lazy as _


class Culture(models.Model):
    class Meta:
        verbose_name = 'Культура'
        verbose_name_plural = 'Культуры'

    name = models.CharField(max_length=250, verbose_name=_('Имя'))
    picture_name = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('Название картинки'))
    ordering = models.IntegerField(default=0, verbose_name=_('Сортировка'))


