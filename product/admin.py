from django.contrib import admin

from .models import (
    ProductGroup,
    Product,
    ChemGroupForProduct,
    ActiveSubstanceForProduct,
    CultureForProduct,
    MicroelementForProduct
)

admin.site.register(ProductGroup)
admin.site.register(Product)
admin.site.register(ChemGroupForProduct)
admin.site.register(ActiveSubstanceForProduct)
admin.site.register(CultureForProduct)
admin.site.register(MicroelementForProduct)
