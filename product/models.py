from django.db import models
from django.utils.translation import gettext_lazy as _

from catalog.models import Barcode, ChemicalGroup, ActiveSubstance, Unit, Microelement
from culture.models import Culture


class ProductGroup(models.Model):
    class Meta:
        verbose_name = 'Группа продукта'
        verbose_name_plural = 'Группы продуктов'

    name = models.CharField(max_length=100, verbose_name=_('Имя'))
    ordering = models.IntegerField(default=0, verbose_name=_('Сортировка'))


class Product(models.Model):
    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    name = models.CharField(max_length=256, verbose_name=_('Имя'))
    picture_name = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('Название картинки'))
    link = models.CharField(max_length=256, verbose_name=_('Ссылка на веб-сайт продукта'))
    description = models.TextField(verbose_name=_('Описание'))
    active = models.BooleanField(default=True, verbose_name=_('Активация'))
    temperatureFrom = models.IntegerField(null=True, verbose_name=_('Температура ОТ'))
    temperatureTo = models.IntegerField(null=True, verbose_name=_('Температура ДО'))
    insectoFungisize = models.BooleanField(default=False, verbose_name=_('Насекомое'))
    product_group = models.ForeignKey(ProductGroup, on_delete=models.CASCADE, verbose_name=_('Ссылка на группу'))
    barcode = models.ForeignKey(Barcode, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на штрих-код'))


class ChemGroupForProduct(models.Model):
    class Meta:
        verbose_name = 'Продукт сгруппированный по химическому классу'
        verbose_name_plural = 'Продукты сгруппированные по химическому классу'

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на продукт'))
    chem_group = models.ForeignKey(ChemicalGroup, on_delete=models.CASCADE,
                                   null=True, verbose_name=_('Ссылка на химическую группу'))


class ActiveSubstanceForProduct(models.Model):
    class Meta:
        verbose_name = 'Продукт сгруппированный по активному веществу'
        verbose_name_plural = 'Продукты сгруппированные по активным веществам'

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на продукт'))
    active_substance = models.ForeignKey(ActiveSubstance, on_delete=models.CASCADE,
                                   null=True, verbose_name=_('Ссылка на активное вещество'))
    quantity = models.CharField(max_length=50, verbose_name=_('Количество'))
    unit_name = models.CharField(max_length=30, verbose_name=_('unitName'))
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на ед. измерения'))


class CultureForProduct(models.Model):
    class Meta:
        verbose_name = 'Продукт сгруппированный по культуре'
        verbose_name_plural = 'Продукты сгруппированный по культурам'

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на продукт'))
    culture = models.ForeignKey(Culture, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на культуру'))


class MicroelementForProduct(models.Model):
    class Meta:
        verbose_name = 'Продукт сгруппированный по микроэлементу'
        verbose_name_plural = 'Продукты сгруппированный по микроэлементам'

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на продукт'))
    microelement = models.ForeignKey(Microelement, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на микроэлемет'))
    quantity = models.CharField(max_length=20, verbose_name=_('Количество'))
    unit_name = models.CharField(max_length=30, verbose_name=_('unitName'))
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True, verbose_name=_('Ссылка на ед. измерения'))

