from django.contrib import admin

from .models import (
    ActiveSubstance,
    ChemicalGroup,
    Microelement,
    PackingName,
    Unit,
    Packing,
    PerformMethods,
    PreparativeForm,
    Barcode
)

admin.site.register(ActiveSubstance)
admin.site.register(ChemicalGroup)
admin.site.register(Microelement)
admin.site.register(PackingName)
admin.site.register(Unit)
admin.site.register(Packing)
admin.site.register(PerformMethods)
admin.site.register(PreparativeForm)
admin.site.register(Barcode)

