from django.db import models
from django.utils.translation import gettext_lazy as _


class ActiveSubstance(models.Model):
    class Meta:
        verbose_name = 'Активное вещество'
        verbose_name_plural = 'Активные вещества'

    name = models.CharField(max_length=100, verbose_name=_('Имя'))


class Barcode(models.Model):
    class Meta:
        verbose_name = 'Штрих-код'
        verbose_name_plural = 'Штрих-коды'

    code = models.CharField(max_length=2, blank=True, verbose_name=_('Код'))


class ChemicalGroup(models.Model):
    class Meta:
        verbose_name = 'Химическая группа'
        verbose_name_plural = 'Химические группы'

    name = models.CharField(max_length=200, verbose_name=_('Имя'))


class Microelement(models.Model):
    class Meta:
        verbose_name = 'Микроэлемент'
        verbose_name_plural = 'Микроэлементы'

    symbol = models.CharField(max_length=10, verbose_name=_('Символ'))


class PackingName(models.Model):
    class Meta:
        verbose_name = 'Название упаковки'
        verbose_name_plural = 'Название упаковок'

    name = models.CharField(max_length=100, verbose_name=_('Имя'))


class Unit(models.Model):
    class Meta:
        verbose_name = 'Еденица измерения'
        verbose_name_plural = 'Еденицы измерений'

    name = models.CharField(max_length=40, verbose_name=_('Имя'))


class Packing(models.Model):
    class Meta:
        verbose_name = 'Упаковка'
        verbose_name_plural = 'Упаковки'

    packing_name = models.ForeignKey(PackingName, on_delete=models.CASCADE, verbose_name=_('Ссылка на название упаковки'))
    quantity = models.CharField(max_length=20, verbose_name=_('Количество'))
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, verbose_name=_('Ссылка на еденицу измерения'))


class PerformMethods(models.Model):
    class Meta:
        verbose_name = 'Метод обработки'
        verbose_name_plural = 'Методы обработок'

    name = models.TextField(verbose_name=_('Имя'))


class PreparativeForm(models.Model):
    class Meta:
        verbose_name = 'Препаративная форма'
        verbose_name_plural = 'Препаративные формы'

    name = models.CharField(max_length=200, verbose_name=_('Имя'))
